#!/bin/bash
echo '' 
echo '******************* Present state ****************************'
sudo docker ps -a 
sudo docker images
echo '' 
echo '******************* Cleaning *********************************'
sudo docker container stop $(sudo docker ps -aq)
sudo docker container prune -f 
sudo docker rmi $(sudo docker images -q)
echo '' 
echo '******************* New state ********************************'
sudo docker ps -a 
sudo docker images
